## Current images
| Image                                                                                                                                                                                                                                   | Version    |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| vendor.img                                                                                                                                                                                                                              | V12.0.3.0 |
| abl.img<br>bluetooth.img<br>cmnlib.img<br>cmnlib64.img<br>devcfg.img<br>dsp.img<br>hyp.img<br>imagefv.img<br>keymaster.img<br>modem.img<br>qupfw.img<br>rpm.img<br>storsec.img<br>tz.img<br>uefisecapp.img<br>xbl.img<br>xbl_config.img | V12.0.3.0  |

